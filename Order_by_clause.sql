#Mengurutkan data
SELECT * FROM product
ORDER BY price ASC, id DESC;

SELECT id, category, price, name FROM product
ORDER BY category ASC, price DESC;