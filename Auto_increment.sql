#Membuat tabel dengan auto increment
CREATE TABLE admin
(
    id INT NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    PRIMARY KEY(id)
) ENGiNe = innoDB

DESCRIBE admin

#Menambah data ke tabel dengan auto increment
INSERT INTO admin(first_name, last_name)
VALUE ('Eko','Khannedy'),
        ('Budi', 'NUgraha'),
        ('Joko','Morro');

SELECT * FROM admin ORDER BY id

#Melihat id terahir
SELECT LAST_INSERT_ID();