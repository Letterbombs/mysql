#Operator perbandingan

OPERATOR        KETERANGAN
=               sama dengan
<> atau !=      tidak sama dengan
<               kurang dari
<=              kurang dari
>               lebih dari
>=              lebih dari atau sama dengan

#Mencari data dengan operator perbandingan
SELECT * FROM product WHERE quantity > 50

SELECT * FROM product WHERE quantity >= 100

SELECT * FROM product WHERE category != 'Makanan'

SELECT * FROM product WHERE category <> 'Minuman'

#AND dan OR operator
SELECT * FROM product WHERE quantity > 50 AND price = 20000

SELECT * FROM product WHERE category = 'Makanan' AND price = 20000

SELECT * FROM product WHERE quantity > 100 OR price =15000

SELECT * FROM product WHERE quantity > 50 AND price = 20000

SELECT * FROM product WHERE quantity > 100 OR (price =15000 AND category = 'Makanan')

#Like operator
SELECT * FROM product WHERE name LIKE '%mie%'

#Null operator
SELECT * FROM product WHERE description IS NULL

SELECT * FROM product WHERE description IS NOT NULL

#Between operator
SELECT * FROM product WHERE price BETWEEN 10000 AND 30000

SELECT * FROM product WHERE price NOT BETWEEN 10000 AND 30000

#In operator
SELECT * FROM product WHERE category IN ('Makanan','Minuman')

SELECT * FROM product WHERE category NOT IN ('Makanan','Minuman')


