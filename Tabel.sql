#Membuat tabel

create table barang(
    id INT,
    nama VARCHAR(100),
    harga INT,
    jumlah INT
)ENGINE = InnoDB;

#Melihat Struktur tabel

desc barang;

#Mengubah tabel
Alter Table barang

    #Menambah kolom
    ADD COLUMN nama_column TEXT,

    #Menghapus kolom
    DROP COLUMN nama,

    #Merubah nama kolom
    RENAME COLUMN nama TO nama_baru,

    #Mengedit jenis data dan menggeser
        MODIFY nama VARCHAR(100) AFTER jumlah,
        MODIFY nama VARCHAR(100) First;

#Null
#Agar tidak NULL gunakan 
NOT NULL

#atau jika sudah membuat tabel gunakan
ALTER TABLE barang
MODIFY nama INT NOT NULL

#Default value
ALTER TABLE barang
MODIFY nama INT NOT NULL DEFAULT 0
MODIFY waktu_dibuat timestamp NOT NULL default CURRENT_TIMESTAMP

#menghapus ulang tabel
TRUNCATE barang

#menghapus permanen
DROP TABLE barang
