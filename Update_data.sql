#Menambah kolom kategori
ALTER TABLE product
ADD COLUMN category ENUM('Makanan', 'Minuman', 'Lain-lain')
AFTER name;

#Mengubah satu kolom
UPDATE product
SET category = 'Makanan'
WHERE id = 'P0001'

#Mengubah beberapa kolom
UPDATE product
SET category = 'Makanan',
    description = 'Mie Ayam Original + Ceker'
WHERE id = 'P0003'

#Mengubah dengan value di kolom
UPDATE product
SET price = price + 5000
WHERE id = 'P0005'


desc product
select * from product